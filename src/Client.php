<?php

namespace mengsen\consul;

class Client
{

    public $config;
    public function __construct($config = [])
    {
        if (empty($config)) {
            throw new \Exception('配置不能为空');
        }
        $this->config = $config;
    }

    public function execute($url = '', $param = [], $header = [])
    {
        $client = new \GuzzleHttp\Client();
        $result =  $client->request($param['method'] ?? 'get', $this->config['host'] . $url, ['json' => json_encode($param), 'headers' => $header]);
        return json_decode($result->getBody()->getContents(), true);
    }
}
