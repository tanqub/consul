### consul
consul、服务发现、服务注册、服务管理

### demo

    use mengsen\consul\Client;

    class test
    {
        
        public function index()
        {   
            $conf = config('consul'); //获取配置
            $app = new Client($conf);
            $result = $app->execute(
                'v1/catalog/services',
                [
                    'method' => 'post', //默认get
                ]
            );
        }
    }